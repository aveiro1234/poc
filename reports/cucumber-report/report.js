$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("a.feature");
formatter.feature({
  "line": 2,
  "name": "AUT_LOGIN",
  "description": "",
  "id": "aut-login",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Tag1"
    }
  ]
});
formatter.scenario({
  "line": 7,
  "name": "Login with valid data",
  "description": "",
  "id": "aut-login;login-with-valid-data",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "login page",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "on entering valid login crediatials and click sign in button",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Home page is displyed",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "Then Home page should contain Home",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "Then Home page should contain Requests",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "Then Home page should contain Reports",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "Then Home page should contain Feedback",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "Then Home page should contain Logged User Name",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "Then Home page should contain Logged Pending Approvals",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "Then Home page should contain Logged My Submissions",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "Then Home page should contain Logged My Historical Actions",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "Then Home page should contain Logged My Projects",
  "keyword": "And "
});
formatter.match({
  "location": "test1.login_page()"
});
formatter.result({
  "duration": 13505468500,
  "status": "passed"
});
formatter.match({
  "location": "test1.on_entering_valid_login_crediatials_and_click_sign_in_button()"
});
formatter.result({
  "duration": 470420100,
  "status": "passed"
});
formatter.match({
  "location": "test1.home_page_is_displyed()"
});
formatter.result({
  "duration": 175400,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Home()"
});
formatter.result({
  "duration": 1260187100,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Requests()"
});
formatter.result({
  "duration": 248663900,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Reports()"
});
formatter.result({
  "duration": 271482400,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Feedback()"
});
formatter.result({
  "duration": 325560900,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Logged_User_Name()"
});
formatter.result({
  "duration": 583295700,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Logged_Pending_Approvals()"
});
formatter.result({
  "duration": 462967200,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Logged_My_Submissions()"
});
formatter.result({
  "duration": 424450000,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Logged_My_Historical_Actions()"
});
formatter.result({
  "duration": 468436900,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Home_page_should_contain_Logged_My_Projects()"
});
formatter.result({
  "duration": 509122000,
  "status": "passed"
});
formatter.uri("b.feature");
formatter.feature({
  "line": 2,
  "name": "AUT_LOGIN1",
  "description": "",
  "id": "aut-login1",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Tag2"
    }
  ]
});
formatter.scenario({
  "line": 7,
  "name": "Login with valid data1",
  "description": "",
  "id": "aut-login1;login-with-valid-data1",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "login page",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "on entering valid login crediatials and click sign in button and click on drop down and add  start and end date and click filter",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Result set is displyed in Awaiting Allocations Approvals",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "Then count of set should displyed in Awaiting Allocations Approvals",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "Then in case of no  pending reports mesage should dispalyed",
  "keyword": "And "
});
formatter.match({
  "location": "test1.login_page()"
});
formatter.result({
  "duration": 11521294100,
  "status": "passed"
});
formatter.match({
  "location": "test1.on_entering_valid_login_crediatials_and_click_sign_in_button_and_click_on_drop_down_and_add_start_and_end_date_and_click_filter()"
});
formatter.result({
  "duration": 4030516900,
  "status": "passed"
});
formatter.match({
  "location": "test1.result_set_is_displyed_in_Awaiting_Allocations_Approvals()"
});
formatter.result({
  "duration": 5485471100,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_count_of_set_should_displyed_in_Awaiting_Allocations_Approvals()"
});
formatter.result({
  "duration": 507472700,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_in_case_of_no_pending_reports_mesage_should_dispalyed()"
});
formatter.result({
  "duration": 520503700,
  "status": "passed"
});
formatter.uri("c.feature");
formatter.feature({
  "line": 2,
  "name": "AUT_LOGIN_SUP",
  "description": "",
  "id": "aut-login-sup",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Tag3"
    }
  ]
});
formatter.scenario({
  "line": 4,
  "name": "Login with valid data  and change sup",
  "description": "",
  "id": "aut-login-sup;login-with-valid-data--and-change-sup",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "Enter valid login credential and login in and hovering over requests and select supervison change and enter empid or emp name in new page",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "verify is able to navigate to change page",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Then Display emp photo",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Then Display Designation",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Then Display Join date",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Then Display Supervison",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "Then Display clickeble change icon",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "Then Display Supervison change date",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "Then Display Direct Reportees Grid",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "Then Display continue button",
  "keyword": "And "
});
formatter.match({
  "location": "test1.login_page()"
});
formatter.result({
  "duration": 10576738300,
  "status": "passed"
});
formatter.match({
  "location": "test1.enter_valid_login_credential_and_login_in_and_hovering_over_requests_and_select_supervison_change_and_enter_empid_or_emp_name_in_new_page()"
});
formatter.result({
  "duration": 2500644400,
  "status": "passed"
});
formatter.match({
  "location": "test1.verify_is_able_to_navigate_to_change_page()"
});
formatter.result({
  "duration": 6587061100,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_emp_photo()"
});
formatter.result({
  "duration": 701622700,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_Designation()"
});
formatter.result({
  "duration": 674732900,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_Join_date()"
});
formatter.result({
  "duration": 861942900,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_Supervison()"
});
formatter.result({
  "duration": 712941800,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_clickeble_change_icon()"
});
formatter.result({
  "duration": 674380100,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_Supervison_change_date()"
});
formatter.result({
  "duration": 991051100,
  "status": "passed"
});
formatter.match({
  "location": "test1.then_Display_Direct_Reportees_Grid()"
});
formatter.result({
  "duration": 6028586900,
  "error_message": "java.lang.AssertionError: The following asserts failed:\n\texpected [false] but found [true]\r\n\tat org.testng.asserts.SoftAssert.assertAll(SoftAssert.java:43)\r\n\tat STEP_DEF.test1.then_Display_Direct_Reportees_Grid(test1.java:717)\r\n\tat ✽.And Then Display Direct Reportees Grid(c.feature:14)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "test1.then_Display_continue_button()"
});
formatter.result({
  "status": "skipped"
});
});