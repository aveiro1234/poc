package TEST_RUNNER;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features="FEATURES",tags= {"@Tag1,@Tag2,@Tag3"},glue="STEP_DEF",plugin="html:reports/cucumber-report")
public class testrunner extends AbstractTestNGCucumberTests {
	
}
