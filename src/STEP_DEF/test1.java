package STEP_DEF;

import java.text.SimpleDateFormat;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.it.Date;

public class test1 {
	
	static ExtentHtmlReporter reporter = new ExtentHtmlReporter("./Report/extent2.html");  
	static ExtentReports extent = new ExtentReports();
	

	
	public static void extent_report(String test_case_name, String info, String pass, String fail , String act, String exp) {
System.out.println("in ext");
		extent.attachReporter(reporter);
		 
		ExtentTest logger = extent.createTest(test_case_name);
		 
		logger.log(Status.INFO, info);
		 
		if(act.equals(exp)) {
		 logger.log(Status.PASS, pass);}  
		 else
		 logger.log(Status.FAIL, fail);
		 
		 extent.flush();
		}
	
	
	
	public static void extent_report1(String test_case_name, String info, String pass, String fail , boolean act, boolean exp) {
		System.out.println("in ext");
				extent.attachReporter(reporter);
				 
				ExtentTest logger = extent.createTest(test_case_name);
				 
				logger.log(Status.INFO, info);
				 
				if(act==exp) {
				 logger.log(Status.PASS, pass);}  
				 else
				 logger.log(Status.FAIL, fail);
				 
				 extent.flush();
				}

	
	static WebDriver dr;
	@Given("^login page$")
	public void login_page() throws Throwable {
		
		
		
		System.out.println("Login page is displayed");  

		System.setProperty("webdriver.chrome.driver","chromedriver_78.exe");
		dr=new ChromeDriver();
		dr.get("https://ra-staging.globallogic.com/login");
		dr.manage().window().maximize();
	  
	}

@When("^on entering valid login crediatials and click sign in button$")
	public void on_entering_valid_login_crediatials_and_click_sign_in_button() throws Throwable {
	   dr.findElement(By.name("username")).sendKeys("arun.pp");
	   dr.findElement(By.name("password")).sendKeys("Amritha@1234");
	   //dr.findElement(By.className("btn btn-primary btn-block btn-lg")).click();
	   dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/input")).click();
	}

	@Then("^Home page is displyed$")
	public void home_page_is_displyed() throws Throwable {
		System.out.println("Home page displyed");
		
	}
	
	/*@Then("^Home page should contain Logged Pending Approvals$")
	public void home_page_should_contain_Logged_Pending_Approvals() throws Throwable {
	
		
		//ul[@id='profile-image-container']/li/a/b
		
		String appr="Pending Approvals";

		WebDriverWait wait=new WebDriverWait(dr,10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(.,'Pending Approvals')]")));
		String appr1=dr.findElement(By.xpath("//h4[contains(.,'Pending Approvals')]")).getText();
		System.out.println("in pending app");
		SoftAssert as=new SoftAssert();
		as.assertEquals(appr,appr1);
		System.out.println(appr1);
		as.assertAll();
		
		
		
		extent_report("TC_1","VERIFICATION OF PENDING APPOVALS IN HOME","VERIFIED","FAILED TO VERIFY",appr,appr1);
		
		
	}*/
	


	@Then("^Then Home page should contain Home$")
	public void then_Home_page_should_contain_Home() throws Throwable {

		   String Home="Home";
		  // String Home1=dr.findElement(By.xpath("/html/body/user-app/app-header/div/div/div/div[2]/ul[1]/li[1]/a/text()")).getText();
		 //  dr.findElement(By.linkText("/home")).click();
		   WebDriverWait wait=new WebDriverWait(dr,6);
		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Home")));
		   String Home1=dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[1]/a")).getText();
		  System.out.println(Home1);
		  /* SoftAssert as =new SoftAssert();
		   as.assertEquals(Home, Home1);
		   as.assertAll();*/
		   
		   
		   extent_report("TC_1","VERIFICATION OF HOME BUTTON IN HOME","VERIFIED","FAILED TO VERIFY",Home,Home1);
			
	}

@Then("^Then Home page should contain Requests$")
public void then_Home_page_should_contain_Requests() throws Throwable {
	 WebDriverWait wait=new WebDriverWait(dr,6);
	   wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("dropdown-toggle")));
	String req1=dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[2]/a")).getText();
	SoftAssert as=new SoftAssert();
	String req="Requests";
	as.assertEquals(req,req1);
	System.out.println(req1);
	as.assertAll();
	extent_report("TC_1","VERIFICATION OF PREQUESTES IN HOME","VERIFIED","FAILED TO VERIFY",req,req1);
	
	
}

@Then("^Then Home page should contain Reports$")
public void then_Home_page_should_contain_Reports() throws Throwable {

	WebDriverWait wait=new WebDriverWait(dr,6);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("dropbtn")));
	
	String rep1=dr.findElement(By.xpath("//*[@id=\"ramenu\"]/li[3]/a")).getText();
	
	SoftAssert as=new SoftAssert();
	String rep="Reports";
/*	as.assertEquals(rep1,rep);
	System.out.println(rep1);
	as.assertAll();*/
	extent_report("TC_1","VERIFICATION OF REPORTS IN HOME","VERIFIED","FAILED TO VERIFY",rep,rep1);
	
	
	
}

@Then("^Then Home page should contain Feedback$")
public void then_Home_page_should_contain_Feedback() throws Throwable {

	String li="https://docs.google.com/forms/d/e/1FAIpQLSeSlnfGt4oGlE3T-LXEr0w2R4a1EWmrGrdpYkthPuRpFTiNhQ/viewform";
		WebDriverWait wait=new WebDriverWait(dr,6);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("FeedBack")));
	
		String feed1=dr.findElement(By.linkText("FeedBack")).getText();
		
		
		SoftAssert as=new SoftAssert();
		String feed="FeedBack";
		/*as.assertEquals(feed1,feed);
		System.out.println(feed1);
		as.assertAll();*/
		
		
		extent_report("TC_1","VERIFICATION OF FEEDBACK IN HOME","VERIFIED","FAILED TO VERIFY",feed,feed1);
		
}

@Then("^Then Home page should contain Logged User Name$")
public void then_Home_page_should_contain_Logged_User_Name() throws Throwable {

	
	
	WebDriverWait wait=new WebDriverWait(dr,6);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@id='profile-image-container']/li/a/b")));
	
	String log_name1=dr.findElement(By.xpath("//ul[@id='profile-image-container']/li/a/b")).getText();
	
	SoftAssert as=new SoftAssert();
	String log_name="ARUN KRISHNAN PP";
	/*as.assertEquals(true,dr.findElement(By.xpath("//ul[@id='profile-image-container']/li/a/b")).isDisplayed());
	System.out.println(log_name1);
	as.assertAll();
	*/
	boolean act=dr.findElement(By.xpath("//ul[@id='profile-image-container']/li/a/b")).isDisplayed();
	
	extent_report1("TC_1","VERIFICATION OF LOGGED USER NAME IN HOME","VERIFIED","FAILED TO VERIFY",true,act);
	System.out.println("AFTER EXTND");
	
}

@Then("^Then Home page should contain Logged Pending Approvals$")
public void then_Home_page_should_contain_Logged_Pending_Approvals() throws Throwable {
	
	//ul[@id='profile-image-container']/li/a/b
	
	String appr="Pending Approvals";

	WebDriverWait wait=new WebDriverWait(dr,6);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(.,'Pending Approvals')]")));
	String appr1=dr.findElement(By.xpath("//h4[contains(.,'Pending Approvals')]")).getText();
	
/*	SoftAssert as=new SoftAssert();
	as.assertEquals(appr,appr1);
	System.out.println(appr1);
	as.assertAll();*/
	
	
	extent_report("TC_1","VERIFICATION OF PENDING APPOVALS IN HOME","VERIFIED","FAILED TO VERIFY",appr,appr1);
	
}

@Then("^Then Home page should contain Logged My Submissions$")
public void then_Home_page_should_contain_Logged_My_Submissions() throws Throwable {

	String my_sub="My Submissions";
	WebDriverWait wait=new WebDriverWait(dr,6);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(.,'My Submissions')]")));
	String my_sub1=dr.findElement(By.xpath("//h4[contains(.,'My Submissions')]")).getText();
	
	/*SoftAssert as=new SoftAssert();
	as.assertEquals(my_sub1,my_sub);
	System.out.println(my_sub1);
	as.assertAll();*/

	extent_report("TC_1","VERIFICATION OF MY SUBMISSIONs IN HOME","VERIFIED","FAILED TO VERIFY",my_sub,my_sub1);
	

}



@Then("^Then Home page should contain Logged My Historical Actions$")
public void then_Home_page_should_contain_Logged_My_Historical_Actions() throws Throwable {


	String my_His="My Historical Actions";
	WebDriverWait wait=new WebDriverWait(dr,6);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(.,'My Historical Actions')]")));
	String my_His1=dr.findElement(By.xpath("//h4[contains(.,'My Historical Actions')]")).getText();
	
/*	SoftAssert as=new SoftAssert();
	as.assertEquals(my_His1,my_His);
	System.out.println(my_His1);
	as.assertAll();*/
	

	extent_report("TC_1","VERIFICATION OF MY HISTORICAL ACTIONS IN HOME","VERIFIED","FAILED TO VERIFY",my_His,my_His1);
	
}

@Then("^Then Home page should contain Logged My Projects$")
public void then_Home_page_should_contain_Logged_My_Projects() throws Throwable {
	String my_pro="My Projects";
	WebDriverWait wait=new WebDriverWait(dr,6);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[contains(.,'My Proje')]")));
	String my_pro1=dr.findElement(By.xpath("//h4[contains(.,'My Proje')]")).getText();
	/*SoftAssert as=new SoftAssert();
	as.assertEquals(my_pro,my_pro1);
	System.out.println(my_pro);
	as.assertAll();*/
	extent_report("TC_1","VERIFICATION OF MY PROJECTS ACTIONS IN HOME","VERIFIED","FAILED TO VERIFY",my_pro,my_pro1);
	
}


	

	
	
	
	
	
	
	
	
	
	

	
	//*******test case 2
	
	
	@When("^on entering valid login crediatials and click sign in button and click on drop down and add  start and end date and click filter$")
	public void on_entering_valid_login_crediatials_and_click_sign_in_button_and_click_on_drop_down_and_add_start_and_end_date_and_click_filter() throws Throwable {
		 
		   WebDriverWait wait=new WebDriverWait(dr,6);
		 dr.findElement(By.name("username")).sendKeys("arun.pp");
		   dr.findElement(By.name("password")).sendKeys("Amritha@1234");
		   //dr.findElement(By.className("btn btn-primary btn-block btn-lg")).click();
		   dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/input")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Impersonation')]")));
		   dr.findElement(By.xpath("//a[contains(text(),'Impersonation')]")).click();
		   
		   
 wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mysearch")));
		   
		   
		   dr.findElement(By.id("mysearch")).sendKeys("1261");
		   
		   dr.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS) ;
			dr.findElement(By.xpath("(//a[contains(@href, '#')])[3]")).click();
		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".mt-5 .caret")));
		   
		   
	dr.findElement(By.cssSelector(".mt-5 .caret")).click();
		

	  
	   wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='mat-input-0']")));
	   
	   dr.findElement(By.xpath("//input[@id='mat-input-0']")).click();
	   dr.findElement(By.xpath("//input[@id='mat-input-0']")).clear();
	   
	  
	dr.findElement(By.xpath("//input[@id='mat-input-0']")).sendKeys("2017-10-21");
		
	

	   wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='mat-input-1']")));
	   
	  dr.findElement(By.xpath("//input[@id='mat-input-1']")).click();
	   dr.findElement(By.xpath("//input[@id='mat-input-1']")).clear();
	dr.findElement(By.xpath("//input[@id='mat-input-1']")).sendKeys("2019-11-27");
		dr.findElement(By.xpath("//input[@value='Apply Filter']")).click();
	}

	@Then("^Result set is displyed in Awaiting Allocations Approvals$")
	public void result_set_is_displyed_in_Awaiting_Allocations_Approvals() throws Throwable {
		System.out.println("in then 1");
		
		
		try {
			System.out.println("in try");
			
			 String d1="2017-10-21";
			   String d2="2019-11-27";
			  /* SimpleDateFormat  fo=new SimpleDateFormat("dd MM yyyy");
			   Date dd1=fo.format(d1);
			   Date dd2=fo.format(d2);*/
			   
			   java.util.Date dd1=new SimpleDateFormat("yyyy-MM-dd").parse(d1);
			   java.util.Date dd2=new SimpleDateFormat("yyyy-MM-dd").parse(d2);
		dr.findElement(By.xpath("//div[@class='ag-center-cols-container']"));
		System.out.println("befor sddate");
		String sdate=dr.findElement(By.xpath("//*[@id=\"aggrid\"]/div/div[2]/div[1]/div[3]/div[2]/div/div/div/div[7]")).getText();
		System.out.println("afr sddate");
		System.out.println(sdate);
		long date1=dd2.getTime()-dd1.getTime();
		System.out.println("*********");
		System.out.println(date1);
		
		System.out.println(TimeUnit.DAYS.convert(date1,TimeUnit.MILLISECONDS));
		
		java.util.Date dd3=new SimpleDateFormat("yyyy-MM-dd").parse(sdate);
		
		long date2=dd3.getTime()-dd1.getTime();
		
		System.out.println(TimeUnit.DAYS.convert(date2,TimeUnit.MILLISECONDS));
		
		if(date2<=date2) {
		/*SoftAssert as=new SoftAssert();
		as.assertEquals(true, true);
		as.assertAll();*/
		
		
		
		extent_report1("TC_2","VERIFICATION RESULT SET DISPLAYED WITHING FILTERED CRITERIA","VERIFIED","FAILED TO VERIFY",true,true);
		
			
		}
		else
		{
			/*SoftAssert as=new SoftAssert();
			as.assertEquals(true, true);
			as.assertAll();*/
			extent_report1("TC_2","VERIFICATION RESULT SET DISPLAYED WITHING FILTERED CRITERIA","VERIFIED","FAILED TO VERIFY",true,false);
			
		}
		}
		catch(Exception e) {
			extent_report1("TC_2","VERIFICATION RESULT SET DISPLAYED WITHING FILTERED CRITERIA","VERIFIED","FAILED TO VERIFY",true,false);
			
			
		}
		   
	}

	@Then("^Then count of set should displyed in Awaiting Allocations Approvals$")
	public void then_count_of_set_should_displyed_in_Awaiting_Allocations_Approvals() throws Throwable {
	 boolean a=true;
		//h4[@class='pull-left']
	 SoftAssert as=new SoftAssert();
		try {
		String s=dr.findElement(By.xpath("//h4[@class='pull-left']")).getText();
		System.out.println(s);
		String s1=s.substring(31,32);
		System.out.println(s1);
		int x=Integer.parseInt(s1);
		/*as.assertEquals(true,dr.findElement(By.xpath("//h4[@class='pull-left']")).isDisplayed());
		System.out.println("Count of pending approvals:"+x);
		
		as.assertAll();*/
		
		
		
		extent_report1("TC_2","VERIFICATION OF COUNT OF RESULT","VERIFIED","FAILED TO VERIFY",true,dr.findElement(By.xpath("//h4[@class='pull-left']")).isDisplayed());
		
		//System.out.println("Count of pending approvals:"+x);
		
		
		
		}
		catch(Exception e) {
			//SoftAssert as=new SoftAssert();
			/*as.assertEquals(true, false);
			as.assertAll();*/
			extent_report1("TC_2","VERIFICATION RESULT SET DISPLAYED WITHING FILTERED CRITERIA","VERIFIED","FAILED TO VERIFY",true,false);
		}
		
		
		
	
	
	}

	@Then("^Then in case of no  pending reports mesage should dispalyed$")
	public void then_in_case_of_no_pending_reports_mesage_should_dispalyed() throws Throwable {

		/*   WebDriverWait wait=new WebDriverWait(dr,6);
		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("alert alert-danger")));
		   
		   
		String s=dr.findElement(By.className("alert alert-danger")).getText();
		System.out.println(s);*/
		try {
		System.out.println("in pending");
		String s=dr.findElement(By.xpath("//strong[contains(text(),'No approvals are pending')]")).getText();
		boolean x=false;
		if(dr.findElement(By.xpath("//strong[contains(text(),'No approvals are pending')]")).isDisplayed());
		{
			
		/*	SoftAssert as=new SoftAssert();
			as.assertEquals(s,"No approvals are pending");
			as.assertAll();*/
			
			
			extent_report("TC_2","VERIFICATION OF NO PENDING REPORTS MESSAGE","VERIFIED","FAILED TO VERIFY",s,"No approvals are pending");
		}
		}catch(Exception e){
			/*SoftAssert as=new SoftAssert();
			as.assertEquals(true,true);
			as.assertAll();*/
			extent_report1("TC_2","VERIFICATION OF NO PENDING REPORTS MESSAGE","VERIFIED","FAILED TO VERIFY",true,false);
		}
		
		
	}


	
	
	
	
	//test case 7

	@When("^Enter valid login credential and login in and hovering over requests and select supervison change and enter empid or emp name in new page$")
	public void enter_valid_login_credential_and_login_in_and_hovering_over_requests_and_select_supervison_change_and_enter_empid_or_emp_name_in_new_page() throws Throwable {
		
		System.out.println("in when");
		dr.findElement(By.name("username")).sendKeys("arun.pp");
		   dr.findElement(By.name("password")).sendKeys("Amritha@1234");
		   //dr.findElement(By.className("btn btn-primary btn-block btn-lg")).click();
		   dr.findElement(By.xpath("/html/body/user-app/ng-component/div[2]/form/input")).click();
		
		

		   WebDriverWait wait=new WebDriverWait(dr,20);
		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("dropdown-toggle")));
		   
		   
		   
		   Actions actions = new Actions(dr);
	        //Retrieve WebElement 'Music' to perform mouse hover 
	     WebElement menuOption = dr.findElement(By.className("dropdown-toggle"));
	     //Mouse hover menuOption 'Music'
	     actions.moveToElement(menuOption).perform();
		   
	     //WebElement subMenuOption = dr.findElement(By.linkText("Supervisor Change")); 
	     //Mouse hover menuOption 'Rock'
	     dr.findElement(By.linkText("Supervisor Change")).click();
	    // actions.moveToElement(subMenuOption).click();
	     
		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mysearch")));
		   
		   
		   dr.findElement(By.id("mysearch")).sendKeys("1261");
		   //316557
		   
		  
		   dr.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS) ;
		dr.findElement(By.xpath("(//a[contains(@href, '#')])[3]")).click();
		   
		   
		   
		
		
		
	}
	
	
	@Then("^verify is able to navigate to change page$")
	public void verify_is_able_to_navigate_to_change_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
		
		

		 WebDriverWait wait=new WebDriverWait(dr,20);
		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("avatar")));
		   
		   
		   
		   String url="https://ra-staging.globallogic.com/hractions/bulkrmchange";
		   
		   
		   String url2=dr.getCurrentUrl();
		   
		   
		   
		  /* SoftAssert as=new SoftAssert();
		   
		   as.assertEquals(url,url2);
		   as.assertAll();*/
		   
		   extent_report("TC_7","VERIFICATION OF NAVIGATION TO SUPERVISOR CHANGE PAGE","VERIFIED","FAILED TO VERIFY",url,url2);
		   
		   
		   
	}

	@Then("^Then Display emp photo$")
	public void then_Display_emp_photo() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
		
		

		SoftAssert as=new SoftAssert();
		//System.out.println(ExpectedConditions.visibilityOfElementLocated(By.className("avatkar")));
		//as.assertEquals(arg0, arg1);
		//as.assertAll(ExpectedConditions.visibilityOfElementLocated(By.className("avatar")));
	
	
		/*	as.assertEquals(true,dr.findElement(By.className("avatar")).isDisplayed());
	as.assertAll();*/
		
		boolean t=dr.findElement(By.className("avatar")).isDisplayed();
		  extent_report1("TC_7","VERIFICATION OF PROFILE PHOTO IS DISPLAYED IN SUPERVISOR CHANGE PAGE","VERIFIED","FAILED TO VERIFY",true,t);
		   
	}

	@Then("^Then Display Designation$")
	public void then_Display_Designation() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
		
		/*	SoftAssert as=new SoftAssert();
		as.assertEquals(true,dr.findElement(By.xpath("//label[contains(text(),'Designation:')]")).isDisplayed());
		as.assertAll();*/
				
				/*SoftAssert as=new SoftAssert();
				as.assertEquals("Designation: Manager",dr.findElement(By.xpath("//p[contains(text(),'Manager')]")).getText());
				as.assertAll();*/
				extent_report("TC_7","VERIFICATION OF DESIGNATION  IS DISPLAYED IN SUPERVISOR CHANGE PAGE","VERIFIED","FAILED TO VERIFY","Designation: Manager",dr.findElement(By.xpath("//p[contains(text(),'Manager')]")).getText());
				   
		
		
		
		
	}

	@Then("^Then Display Join date$")
	public void then_Display_Join_date() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
		
		/*SoftAssert as=new SoftAssert();
		as.assertEquals(true,dr.findElement(By.xpath("//label[contains(text(),'Joining Date:')]")).isDisplayed());
		as.assertAll();*/
		
		
		
		
		/*SoftAssert as=new SoftAssert();
		as.assertEquals("Joining Date: 2006-08-10",dr.findElement(By.xpath("//p[contains(text(),'2006-08-10')]")).getText());
		as.assertAll();*/
		extent_report1("TC_7","VERIFICATION OF JOINING DATE IS DISPLAYED IN SUPERVISOR CHANGE PAGE","VERIFIED","FAILED TO VERIFY",true,dr.findElement(By.xpath("//p[contains(text(),'2006-08-10')]")).isDisplayed());
		   
		
		
		
	}

	@Then("^Then Display Supervison$")
	public void then_Display_Supervison() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
		
		
		
		/*SoftAssert as=new SoftAssert();
		as.assertEquals("Supervisor: Anurag Agrawal (3411) Change",dr.findElement(By.xpath("//p[contains(text(),'Anurag Agrawal (3411)')]")).getText());
		as.assertAll();*/
		
		
		extent_report1("TC_7","VERIFICATION OF SUPERVISOR NAME  IS DISPLAYED IN SUPERVISOR CHANGE PAGE","VERIFIED","FAILED TO VERIFY",true,dr.findElement(By.xpath("//p[contains(text(),'Anurag Agrawal (3411)')]")).isDisplayed());
		   
		
	}

	@Then("^Then Display clickeble change icon$")
	public void then_Display_clickeble_change_icon() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
		
		/*SoftAssert as=new SoftAssert();
		as.assertEquals(true,dr.findElement(By.xpath("//button[@class='badge change']")).isDisplayed());
		as.assertAll();*/
		extent_report1("TC_7","VERIFICATION OF CLICKEBELE CHANGE ICON IS DISPLAYED IN SUPERVISOR CHANGE PAGE","VERIFIED","FAILED TO VERIFY",true,dr.findElement(By.xpath("//button[@class='badge change']")).isDisplayed());
		   
		
	}

	@Then("^Then Display Supervison change date$")
	public void then_Display_Supervison_change_date() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		
		/*SoftAssert as=new SoftAssert();
		as.assertEquals(true,dr.findElement(By.xpath("//p[contains(text(),'2018-07-01')]")).isDisplayed());
		as.assertAll();*/
		extent_report1("TC_7","VERIFICATION OF SUPERVISOR CHANGE DATE IS DISPLAYED IN SUPERVISOR CHANGE PAGE","VERIFIED","FAILED TO VERIFY",true,dr.findElement(By.xpath("//p[contains(text(),'2018-07-01')]")).isDisplayed());
		   
		
		
	     
	}

	@Then("^Then Display Direct Reportees Grid$")
	public void then_Display_Direct_Reportees_Grid() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	
		/*//ag-header-cell-text
		String s=dr.findElement(By.className("ag-header-cell-text")).getText();
		System.out.println(s);
		String s1="Employee Name";
		
		
		dr.findElement(By.className("ag-header-cell-text")).click();
		dr.findElement(By.className("ag-header-cell-text")).click();
		
		String name_exp_emp="Vishnu Kumar";
		String name_emp=dr.findElement(By.xpath("//span[contains(text(),'Vishnu Kumar')]")).getText();
		System.out.println(name_emp);
		SoftAssert as=new SoftAssert();
		as.assertEquals(name_exp_emp,name_emp);
		as.assertAll();
		*/
		SoftAssert as=new SoftAssert();
		try{
			
			dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[3]"));
			
			/*as.assertEquals(true,dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[3]")).isDisplayed());
			
			
			as.assertAll();*/
			extent_report1("TC_7","VERIFICATION OF DIRECT REPORTIEE GRID IS DISPLAYED IN SUPERVISOR CHANGE PAGE","VERIFIED","FAILED TO VERIFY",true,dr.findElement(By.xpath("//*[@id=\"tabA\"]/div/form/div/div[2]/div[3]")).isDisplayed());
			   
			
			
			
		}catch(Exception e) {
			
			as.assertEquals(true,false);
			as.assertAll();
			
		}
		
		
		
		

		
		
	}

	@Then("^Then Display continue button$")
	public void then_Display_continue_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	     
	
	
		WebDriverWait wait=new WebDriverWait(dr,20);
		   wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn btn-primary wide ml-30 pull-right']")));
		   
	//dr.findElement(By.className("btn btn-primary wide ml-30 pull-right")).click();
		  /* SoftAssert as=new SoftAssert();
			as.assertEquals(true,dr.findElement(By.xpath("//button[@class='btn btn-primary wide ml-30 pull-right']")).isDisplayed());
			as.assertAll();
			*/
			
			extent_report1("TC_7","VERIFICATION OF CONTINUE BUTTON IN SUPERVISOR CHANGE PAGE","VERIFIED","FAILED TO VERIFY",true,dr.findElement(By.xpath("//button[@class='btn btn-primary wide ml-30 pull-right']")).isDisplayed());
			
			
	}



	
	
	
	
	
	
	


}
